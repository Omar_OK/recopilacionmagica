package com.omarok.magialarecopilacin.ui.main;

import androidx.lifecycle.ViewModelProviders;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.omarok.magialarecopilacin.CardAdapter;
import com.omarok.magialarecopilacin.CartaMagica;
import com.omarok.magialarecopilacin.DBCartasAPI;
import com.omarok.magialarecopilacin.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainFragment extends Fragment {

    private ArrayList<CartaMagica> items;
    //private ArrayAdapter<CartaMagica> adapter;
    private CardAdapter adapter;




    private MainViewModel mViewModel;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        refresh();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.main_fragment, container, false);

        items = new ArrayList<>();

        ListView listViewMagic = view.findViewById(R.id.listViewMagic);


            adapter = new CardAdapter(
                getContext(),
                R.layout.list_view_magic_row,
                    items
            );

        listViewMagic.setAdapter(adapter);

        return view;
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        // TODO: Use the ViewModel
    }


    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<CartaMagica>> {
        @Override
        protected ArrayList<CartaMagica> doInBackground(Void... voids) {
            DBCartasAPI api = new DBCartasAPI();
            ArrayList<CartaMagica> cards = api.getCartes();
            return cards;
        }

        @Override
        protected void onPostExecute(ArrayList<CartaMagica> cartas) {
            adapter.clear();
            for (CartaMagica carta : cartas) {
                adapter.add(carta);
            }
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_cartas_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            refresh();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void refresh() {

        RefreshDataTask task = new RefreshDataTask();
        task.execute();

    }

}

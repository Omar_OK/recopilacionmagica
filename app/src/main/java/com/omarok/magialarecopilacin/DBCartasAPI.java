package com.omarok.magialarecopilacin;

import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class DBCartasAPI {

    private final String BASE_URL = "https://api.magicthegathering.io";

    public ArrayList<CartaMagica> getCartes() {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("v1")
                .appendPath("cards")
                .build();
        String url = builtUri.toString();

        return doCall(url);
    }

   /* String getProximesEstrenes(String pais) {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("card")
                .appendPath("upcoming")
                .appendQueryParameter("region", pais)
                .build();
        String url = builtUri.toString();

        return doCall(url);
    } */

    private ArrayList<CartaMagica> doCall(String url) {
        try {
            String JsonResponse = HttpUtils.get(url);
            return processJson(JsonResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

        private ArrayList<CartaMagica> processJson(String jsonResponse) {
        ArrayList<CartaMagica> cards = new ArrayList<>();
        try {
            JSONObject data = new JSONObject(jsonResponse);
            JSONArray jsonCartaMagicas = data.getJSONArray("cards");
            for (int i = 0; i < jsonCartaMagicas.length(); i++) {
                JSONObject jsonCartaMagica = jsonCartaMagicas.getJSONObject(i);

                CartaMagica card = new CartaMagica();
                card.setName(jsonCartaMagica.getString("name"));
                //card.setReleaseDate(jsonCartaMagica.getString("releaseDate"));
                card.setRarity(jsonCartaMagica.getString("rarity"));
                card.setColor(jsonCartaMagica.getString("colors"));
                //card.setFlavorText(jsonCartaMagica.getString("flavor"));

                if (jsonCartaMagica.has("text"))
                    card.setText(jsonCartaMagica.getString("text"));
                card.setId(jsonCartaMagica.getString("id"));

                if (jsonCartaMagica.has("imageUrl"))
                    card.setCardArt(jsonCartaMagica.getString("imageUrl"));

                cards.add(card);
                Log.d("XXXXXXX", "XXXXXXXXXX final processJson");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return cards;
    }

}

package com.omarok.magialarecopilacin;

import java.io.Serializable;

public class CartaMagica implements Serializable {

        private String name;
        private String releaseDate;
        private String flavorText;
        private String cardArt;
        private String rarity;
        private String color;
        private String type;
        private String text;
        private String id;

        //GETTERS

    public String getName() {
        return name;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public String getFlavorText() {
        return flavorText;
    }

    public String getCardArt() {
        return cardArt;
    }

    public String getRarity() {
        return rarity;
    }

    public String getColor() {
        return color;
    }

    public String getType() {
        return type;
    }

    public String getText() {
        return text;
    }



    public String getId() {
        return id;
    }

    //SETTERS


    public void setName(String name) {
        this.name = name;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public void setFlavorText(String flavorText) {
        this.flavorText = flavorText;
    }

    public void setCardArt(String cardArt) {
        this.cardArt = cardArt;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setText(String text) {
        this.text = text;
    }



    public void setId(String id) {
        this.id = id;
    }

    //ToString


    @Override
    public String toString() {
        return "CartaMagica{" +
                "name='" + name + '\'' +
                ", releaseDate='" + releaseDate + '\'' +
                ", flavorText='" + flavorText + '\'' +
                ", cardArt='" + cardArt + '\'' +
                ", rarity='" + rarity + '\'' +
                ", color='" + color + '\'' +
                ", type='" + type + '\'' +
                ", text='" + text + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}

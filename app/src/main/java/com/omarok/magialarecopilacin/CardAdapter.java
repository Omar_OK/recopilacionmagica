package com.omarok.magialarecopilacin;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.omarok.magialarecopilacin.ui.main.MainFragment;

import java.util.List;

public class CardAdapter extends ArrayAdapter<CartaMagica> {
    public CardAdapter(Context context, int resource, List<CartaMagica> objects) {
        super(context, resource, objects);
    }

    public CardAdapter(@NonNull Context context, int resource) {
        super(context, resource);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CartaMagica carta = getItem(position);
        Log.w("XXXX", carta.toString());


        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_view_magic_row, parent, false);
        }

        TextView textViewMagic = convertView.findViewById(R.id.textViewMagic);
        ImageView ivCardArt = convertView.findViewById(R.id.ivCardArt);

        textViewMagic.setText(carta.getName());
        Glide.with(
                getContext()).load(carta.getCardArt()
        ).into(ivCardArt);

        return convertView;
    }
}